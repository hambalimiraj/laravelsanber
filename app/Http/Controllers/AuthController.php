<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('latihan');
    }

    public function welcome(){
        return view('Selamat_Datang');
    }

    public function kirim(Request $request){
        $fname = $request->Fname;
        $lname = $request->Lname;
        return view('Selamat_Datang')->with(['Fname'=>$fname,'Lname'=>$lname]);
    }
}
