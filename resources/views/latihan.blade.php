<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1> Buat Account Baru! </h1>
    <h2> Sign Up Form </h2>

        <form method="POST" action="/kirim">
            @csrf
            <label for="Fname">First Name:</label><br>
            <input type="text" id="Fname" name="Fname"><br>
            <label for="Lname">Last Name:</label><br>
            <input type="text" id="Lname" name="Lname"><br><br>

            <label for="Gender">Gender:</label><br>
              <input type="radio" id="male" name="Gender" value="male">
              <label for="male">Male</label><br>
              <input type="radio" id="female" name="Gender" value="female">
              <label for="female">Female</label><br>
              <input type="radio" id="other" name="Gender" value="other">
              <label for="other">Other</label><br><br>
            
            <label>Nationally:</label><br>
                <select>
                    <option>Indonesian</option>
                    <option>Malaysian</option>
                    <option>Singaporean</option>
                    <option>Australian</option>
                </select><br><br>

            <label>Language Spoken: </label><br>
                <input type="checkbox">Bahasa Indonesia <br>
                <input type="checkbox">English <br>
                <input type="checkbox">Other <br><br>

            <label>Bio:</label><br>
                <textarea cols="25" rows="8"></textarea><br>
            
            <button type="Submit" value="Sign Up">
                Sign Up
            </button>
                        
        </form>        
</body>
</html>